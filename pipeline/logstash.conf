input {
  kafka {
    bootstrap_servers => "${KAFKA_READ_SERVERS:monit-kafkay-ce8cd4fdc8.cern.ch:9093,monit-kafkay-d98673aa25.cern.ch:9093,monit-kafkay-a29ccd22b4.cern.ch:9093}"
    topics => ["${KAFKA_READ_TOPIC:openstack_logs}"]
    client_id => "${KAFKA_CLIENT_ID:openstack-client}"
    group_id => "${KAFKA_GROUP_ID:openstack}"
    auto_commit_interval_ms => "${KAFKA_AUTOCOMMIT_INTERVAL:10000}"
    codec => json
    security_protocol => "SASL_SSL"
    jaas_path => "${KAFKA_JAAS_PATH:/jaas.conf}"
    sasl_kerberos_service_name => "${KAFKA_KRB_SERVICE_NAME}"
    ssl_truststore_location => "${KAFKA_TRUSTSTORE:/truststore.jks}"
    ssl_truststore_password => "${KAFKA_TS_PASS}"
  }
}

filter {
  # Only do the DNS query if there is a request_ip field
  if [data][request_ip] and [data][request_ip] != "-" {
    mutate {
      add_field => { "[data][request_hostname]" => "%{[data][request_ip]}" }
    }

    dns {
      action => "replace"
      hit_cache_ttl => 3600
      hit_cache_size => 100000
      reverse => ["[data][request_hostname]"]
    }
  }

  # Only do the DNS query if there is a request_lb_ip field
  if [data][request_lb_ip] and [data][request_lb_ip] != "-" {
    mutate {
      add_field => { "[data][request_lb_hostname]" => "%{[data][request_lb_ip]}" }
    }

    dns {
      action => "replace"
      hit_cache_ttl => 3600
      hit_cache_size => 100000
      reverse => ["[data][request_lb_hostname]"]
    }
  }

  # Truncate messages over 8K
  truncate {
    #fields => 'message'
    length_bytes => 8192
  }
}

output {
  kafka {
    bootstrap_servers => "${KAFKA_WRITE_SERVERS:monit-kafkay-ce8cd4fdc8.cern.ch:9093,monit-kafkay-d98673aa25.cern.ch:9093,monit-kafkay-a29ccd22b4.cern.ch:9093}"
    topic_id => ["${KAFKA_WRITE_TOPIC:openstack_enr_logs}"]
    codec => json
    compression_type => gzip
    security_protocol => "SASL_SSL"
    jaas_path => "${KAFKA_JAAS_PATH:/jaas.conf}"
    sasl_kerberos_service_name => "${KAFKA_KRB_SERVICE_NAME}"
    ssl_truststore_location => "${KAFKA_TRUSTSTORE:/truststore.jks}"
    ssl_truststore_password => "${KAFKA_TS_PASS}"
  }
}
